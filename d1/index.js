//Query Operators


//Comparison Operators

//$gt / $gte Operator
//db.collectionName.find({ field: { $gt: value } })
db.users.find({ age: { $gt: 76 } })

//$lt / $lte Operator
db.users.find({ age: { $lt: 76 } })

//Not equal operator ($ne)
//db.collectionName.find({ field: { $ne: value } })

db.users.find({ age: { $ne: 76 } })


//$in operator
//It allows to find documents with specific match criteria 
//db.collectionName.find({ field: { $in: value } })
db.users.find({ courses: { $in: ["Sass", "React"] } })




//Logical Query Operator
//or
//db.collectionName.find({ $or: [{ fieldA: valueA }, { fieldB: valueB }] })
db.users.find({ $or: [ { firstName: "Neil" }, { age: { $gt: 30 } } ] })

//and
//db.collectionName.find({ $and: [{ fieldA: valueA }, { fieldB: valueB }] })
db.users.find({ $and: [ { age: { $ne: 82 } }, { age: { $ne: 76 } } ] })




//Evaluation Query Operator
//$regex operator -> used to look for a partial match in a given field
//it is used to search for STRINGS in collection


//db.collectionName.find({ field: {$regex: 'pattern/keyword', $options: '$i' })
//case sensitive
db.users.find({ firstName: {$regex: 'N' } })

//$options with '$i' parameter specifies that we want to carry out search without considering the upper case and lower cases.




//Field Projection
//if you do not want to retrieve all the fields/properties of a document. you can use field projection




//Inclusion
//include/add specific fields only when retrieving documents
//value of 1 -> to denote that the field is being included.
//db.users.find({ criteria }, { field: 1 })
db.users.find({ firstName: "Jane" }, {
	firstName: 1
})



//Exclusion
//0 to exclude a field
//db.users.find({ criteria }, { field: 0 })
db.users.find({ firstName: "Jane" }, {
	firstName: 1,
	_id: 0
})


//querying with embedded documents
db.users.find({ firstName: "Jane" }, {
    
    "contact.phone" : 1,
    _id : 0
    
    })



//Field projection and slice operator
//$slice operator allows us to retrieve portions of element that matches the search criteria

db.users.find({}, {
    
    _id: 0,
    courses: { $slice: [1, 2] } 
    
    } )
